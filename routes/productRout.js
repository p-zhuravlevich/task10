const express = require("express");
const productController = require("../controllers/productController.js");
const productRout = express.Router();
const auth = require ("../auth.js");

productRout.post("/create",auth.verifyToken,auth.isAdmin ,productController.createProduct);
productRout.delete("/:id",auth.verifyToken,auth.isAdmin, productController.deleteProduct);
productRout.put("/:id",auth.verifyToken,auth.isAdmin, productController.updateProduct);
productRout.get("/:id", auth.verifyToken,productController.getProductById);
productRout.get("/get/all", auth.verifyToken,productController.getAllProducts);

module.exports = productRout;
