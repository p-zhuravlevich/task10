const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  name: {
    type: String,
    default: "NoName",
    minlength: 2,
    maxlength: 35
    },
  surname: {
    type: String,
    default: "NoSurname",
    minlength: 2,
    maxlength: 35
    },
  email: { 
    type: String, 
    unique: true
    },
  password: { 
    type: String,
 },
  token: { type: String },
  role: {
    type: String,
    default: "User"
  },
  cart: {
    type: mongoose.Schema.Types.ObjectId, ref: "Cart",
    //autopopulate: true
  },
  orders: [{
      type: mongoose.Schema.Types.ObjectId, ref: "order",
      autopopulate: true
  }]
}, { versionKey: false });

const User = mongoose.model("User", UserSchema);
UserSchema.plugin(require('mongoose-autopopulate'));

module.exports = User;