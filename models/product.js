const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const productScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength: 1,
        maxlength: 30,
        default: "noname"
    },
    category: [{
        type: mongoose.Schema.Types.ObjectId, ref: "Category",
       
    }]
},
    { versionKey: false }
);
const Product = mongoose.model("Product", productScheme);
module.exports = Product;
