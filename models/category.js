const mongoose = require("mongoose");
const Schema = mongoose.Schema;


const categoryScheme = new Schema({
    name: {
        type: String,
        required: true,
        minlength:1,
        maxlength:20,
        default:"noname"
    },
    products :[{
        type: mongoose.Schema.Types.ObjectId, ref:"Product",
    
    }]
},
{versionKey: false }
);

const Category = mongoose.model("Category", categoryScheme);
module.exports = Category;
